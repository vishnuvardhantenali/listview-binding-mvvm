﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MultiColumnListView_MVVM.MVVM;

namespace MultiColumnListView_MVVM.MVVM.ViewModel
{
    internal class ViewModel
    {
        private List<Model.Model> _customerModels = new List<Model.Model>();
        public ViewModel()
        {
            _customerModels.Add(new Model.Model("Vishnu Vardhan", "Tenali", "22-06-1998"));
            _customerModels.Add(new Model.Model("Keerthi", "Tenali", "15-07-2003"));
            _customerModels.Add(new Model.Model("Deepthi Sree", "Gaddam", "05-02-2003"));
            _customerModels.Add(new Model.Model("Mahesh", "Tenali", "11-08-2008"));
        }

        public List<Model.Model> CustomerModels
        {
            get { return _customerModels; }
            set { _customerModels = value; }
        }
    }
}
